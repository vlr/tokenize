import { constants } from "./constants";

const tests = "testSrc";
export function testFiles(buildFolder: string): string {
  return `./${buildFolder}/${tests}/${constants.allJs}`;
}
