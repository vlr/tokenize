import { expect } from "chai";
import { tokenize } from "../src";
import { Token } from "../src/token.type";

describe("full Library", function (): void {
  it("should successfully break input into tokens", function (): void {
    // arrange
    const tokens = [" ", "@", "@@", "\r", "\n", "\r\n", '@"', '"'];

    const source = 'lets.pretend\r\nthis@is some code @"tobe" generated @@\n';

    // act
    const result = tokenize(source, tokens);

    // assert
    const expected: Token[] = [
      { token: "lets.pretend", position: 0 },
      { token: "\r\n", position: 12 },
      { token: "this", position: 14 },
      { token: "@", position: 18 },
      { token: "is", position: 19 },
      { token: " ", position: 21 },
      { token: "some", position: 22 },
      { token: " ", position: 26 },
      { token: "code", position: 27 },
      { token: " ", position: 31 },
      { token: "@\"", position: 32 },
      { token: "tobe", position: 34 },
      { token: "\"", position: 38 },
      { token: " ", position: 39 },
      { token: "generated", position: 40 },
      { token: " ", position: 49 },
      { token: "@@", position: 50 },
      { token: "\n", position: 52 }
    ];
    expect(result).deep.equal(expected);
  });
});
