import { preprocessTokens } from "../src/preprocessTokens";
import { expect } from "chai";

describe("preprocessTokens", function (): void {
    it("should return hashset containing all tokens", function (): void {
        // arrange
        const input = ["a", "as", "fe", "vervv", "asvrae"];

        // act
        const result = preprocessTokens(input);

        // assert
        input.forEach(token => expect(result.keys.has(token)).to.be.true);
    });

    it("should return lengths of tokens", function (): void {
        // arrange
        const input = ["a", "as", "fe", "vervv", "asvrae"];

        // act
        const result = preprocessTokens(input);

        // assert
        expect(result.lengths).to.be.deep.equal([6, 5, 2, 1]);
    });
});
