import { breakdown } from "../src/breakdown";
import { Preprocessed } from "../src/preprocessed.type";
import { expect } from "chai";

describe("breakdown", function (): void {
  it("should find single token", function (): void {
    // arrange
    const map = new Map<string, boolean>();
    map.set(" ", true);

    const preprocessed: Preprocessed = {
      lengths: [1],
      keys: map
    };

    const source = "simple case";

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect(result).deep.equals(["simple", " ", "case"]);
  });

  it("should find two short tokens", function (): void {
    // arrange
    const map = new Map<string, boolean>();
    map.set(" ", true);
    map.set("T", true);

    const preprocessed: Preprocessed = {
      lengths: [1],
      keys: map
    };

    const source = "twoToken case";

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect(result).deep.equals(["two", "T", "oken", " ", "case"]);
  });

  it("should return input if no token present", function (): void {
    // arrange
    const map = new Map<string, boolean>();
    map.set("w", true);

    const preprocessed: Preprocessed = {
      lengths: [1],
      keys: map
    };

    const source = "noToken case";

    // act
    const result = breakdown(source, preprocessed, t => t);

    // assert
    expect(result).deep.equals([source]);
  });
});
