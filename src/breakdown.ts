import { Preprocessed } from "./preprocessed.type";

export type CreateToken<T> = (token: string, position: number) => T;

export function breakdown<T>(source: string, preprocessed: Preprocessed, create: CreateToken<T>): T[] {
  var position: number = 0;
  const tokens: T[] = [];
  var noTokenStart: number = 0;

  function addNoToken(): void {
    const length = position - noTokenStart;
    if (length > 0) {
      const token = source.substr(noTokenStart, length);
      tokens.push(create(token, noTokenStart));
    }
  }

  function findToken(): string {
    for (var length of preprocessed.lengths) {
      var substring = source.substr(position, length);
      if (preprocessed.keys.has(substring)) { return substring; }
    }
    return null;
  }

  while (position < source.length) {
    var token = findToken();
    if (!token) {
      position++;
      continue;
    }

    addNoToken();
    tokens.push(create(token, position));
    position += token.length;
    noTokenStart = position;
  }

  addNoToken();
  return tokens;
}
