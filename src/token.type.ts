export interface Token {
  token: string;
  position: number;
}
